// Jest.config.js
module.exports = {
  // Automatically clear mock calls and instances between every test
  clearMocks: false,
  testEnvironment: 'jsdom',
  // A list of paths to modules that run some code to configure or set up the testing framework before each test
  // setupFilesAfterEnv: ['./jest.setup.js'],
  setupFiles: ['./jest.setup.js'],
  collectCoverage: true,
  // Regex to match absolute imports
  moduleNameMapper: {
    '^@/(.*)$': '<rootDir>/$1',
  },
  testPathIgnorePatterns: ['./node_modules', './lib/shared/*'],
};
